@extends("layouts.app")

@section("content")

    <div class="panel-body">

        <div class="panel-body">

                @include('common.errors')

                @if(Session::has("success"))
                    <div class="alert alert-success">{{ Session::get("success") }}</div>
                @endif

                @if(Session::has("exception"))
                    <div class="alert alert-danger">{{ Session::get("exception") }}</div>
                @endif

                <form action="/reach" method="POST" class="form-horizontal">
                {{ csrf_field() }}

                    <div class="form-group">
                        <label for="task-name" class="col-sm-3 control-label">Tweet Url</label>

                        <div class="col-sm-6">
                            <input type="url" name="url" id="url" class="form-control" value="{{ old('url') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-default">
                                Get Followers
                            </button>
                        </div>
                    </div>
                </form>
        </div>
    </div>

@endsection()