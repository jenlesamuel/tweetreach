<?php

namespace App\Repositories;

use App\Classes\TwitterApiConnection;
use App\Classes\TweetReachException;
use App\TweetReach;

/**
 * Handles data access operations
 *
 * @author Jenle Samuel
 */

class TweetReachRepository
{

    /**
     * @var TwitterApiConnection
     */

    private $apiConnection;

    /**
     * Constructor.
     *
     * @param  TwitterApiConnection  $apiConnection
     */

    public function __construct(TwitterApiConnection $apiConnection)
    {
        $this->apiConnection = $apiConnection;
    }

    /**
     * Gets a collection of recent retweets of a tweet
     *
     * @param  string  $tweetId
     * @return array
     * @throws TweetReachException
     */

    public function getRetweets($tweetId)
    {
        $connection = $this->apiConnection->getConnection();
        $statuses = $connection->get("statuses/retweets/{$tweetId}");

        if ($connection->getLastHttpCode() == 200)
            return $statuses;

        // An error occurred
        if (isset($statuses->errors))
            // Get first error in the array of errors
            $errorMessage = $statuses->errors[0]->message;
        else
            $errorMessage = "An error occurred";

        throw new TweetReachException($errorMessage);
    }

    /**
     * Retrieves TweetReach from database
     * @param  string  $url
     * @return mixed TweetReach|NULL
     */

    public function get($url)
    {
        $collection =  TweetReach::where("url", $url)
            ->get();

        if (! $collection->isEmpty()) return $collection[0];

        return NULL;
    }

    /**
     * Saves a TweetReach
     *
     * @param  TweetReach  $tweetReach
     * @throws TweetReachException
     */

    public function save(TweetReach $tweetReach)
    {
        $bool = $tweetReach->save();
        if ($bool !== TRUE)
            throw new TweetReachException("Error occurred: Could not save record to DB");
    }

    /**
     * Updates TweetReach
     *
     * @param TweetReach $tweetReach
     * @param array $attributes
     * @return TweetReach
     * @throws TweetReachException
     */

    public function update(TweetReach $tweetReach, array $attributes)
    {
        $bool = $tweetReach->update($attributes);

        if ($bool !== TRUE)
            throw new TweetReachException("Error occurred: Could not update record");

        return $tweetReach;
    }

}
