<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TweetReachService;

class IndexController extends Controller
{
    /**
     * @var TweetReachService
     */

    private $tweetReachService;

    public function __construct(TweetReachService $tweetReachService)
    {
        $this->tweetReachService = $tweetReachService;
    }

    public function index(Request $request)
    {

        return view("index");
    }

    public function getReach(Request $request)
    {

        $this->validate($request, [
            'url' => [
                'required',
                'max:255|url',
                function($attribute, $value, $fail)
                {
                    $pattern = "/^https?:\/\/(www.)?twitter.com\/.*\/[\d]*$/";

                    if (! preg_match($pattern, $value))
                        return $fail('Enter a valid tweet url');
                }
            ]
        ]);

        $url = strtolower($request->url);

        try
        {
            $reach = $this->tweetReachService->getReach($url);
            $request->session()->flash("success", "Tweet reach: {$reach}");

            return redirect("/")
                    ->withInput();
        }
        catch(\Exception $e)
        {
            $request->session()->flash("exception", $e->getMessage());

            return redirect("/")
                    ->withInput();
        }

    }

}
