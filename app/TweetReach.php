<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TweetReach extends Model
{
    protected $fillable = ["followers", "url"];
}
