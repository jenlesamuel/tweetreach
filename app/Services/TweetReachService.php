<?php

namespace App\Services;

use App\Repositories\TweetReachRepository;
use Cache;
use Carbon\Carbon;
use App\TweetReach;

class TweetReachService
{
    /**
     * @var TweetReachRepository
     */

    private $tweetReachRepository;

    public function __construct(TweetReachRepository $tweetReachRepository)
    {
        $this->tweetReachRepository = $tweetReachRepository;
    }

    /**
     * Gets tweet reach from cache
     *
     * @param  string  $tweetUrl
     * @return mixed int|NULL
     */

    public function getReachFromCache($tweetUrl)
    {
        $json = Cache::get(md5($tweetUrl));

        if ($json !== NULL)
        {
            $data = json_decode($json, true);

            if (! $this->hasExpired($data["updated_at"]))
                return intval($data["followers"]);
        }

        return NULL;
    }

    /**
     * Saves TweetReach to cache
     *
     * @param  TweetReach  $tweetReach
     */

    public function saveTweetReachToCache(TweetReach $tweetReach)
    {
        $key = md5($tweetReach->url);

        $data = json_encode([
            "followers" => $tweetReach->followers,
            "updated_at" =>$tweetReach->updated_at->toDateTimeString()
        ]);

        Cache::put($key, $data, 120);
    }

    /**
     * Computes the reach of a tweet from the twitter endpoint
     *
     * @param  int $tweetId
     * @return int
     */

    public function getReachFromEndpoint($tweetId)
    {
        $reach = [];
        $retweets = $this->tweetReachRepository->getRetweets($tweetId);

        foreach ($retweets as $retweet)
        {
            $reach[] = $retweet->user->followers_count;
        }

        return array_sum($reach);
    }

    /**
     * Gets tweet id
     *
     * @param $tweetUrl
     * @return string
     */

    public function getTweetId($tweetUrl)
    {
        $i = strrpos($tweetUrl, "/");

        return substr($tweetUrl, $i+1);

    }

    /**
     * Checks if a time is more than two hours away from current time
     *
     * @param  string  $dateTime
     * @return bool
     */

    public function hasExpired($dateTime)
    {
        $cDateTime  = Carbon::createFromFormat("Y-m-d H:i:s", $dateTime)->addHours(2);

        return Carbon::now()->gt($cDateTime);
    }

    /**
     * Gets the reach a tweet
     *
     * @param  string  $tweetUrl
     * @return int
     */

    public function getReach($tweetUrl)
    {
        $tweetUrl = $this->getTweetUrlNoSchemeProtocol($tweetUrl);
        $tweetId = $this->getTweetId($tweetUrl);

        // Attempt to get data from cache
        $reach = $this->getReachFromCache($tweetUrl);
        if ($reach !== NULL) return $reach;

        // Attempt to get data from DB
        $tweetReach = $this->tweetReachRepository->get($tweetUrl);
        if ($tweetReach !== NULL)
        {
            $hasExpired = $this->hasExpired($tweetReach->updated_at->toDateTimeString());

            if ($hasExpired)
            {
                $reach = $this->getReachFromEndpoint($tweetId);
                $tweetReach = $this->tweetReachRepository->update($tweetReach, ["followers" => $reach]);
            }

            $this->saveTweetReachToCache($tweetReach);
            return $tweetReach->followers;
        }

        // Data neither in cache nor DB
        // Get data from endpoint and save to DB and cache
        $reach = $this->getReachFromEndpoint($tweetId);

        $tweetReach = new TweetReach;
        $tweetReach->url = $tweetUrl;
        $tweetReach->followers = $reach;

        $this->tweetReachRepository->save($tweetReach);
        $this->saveTweetReachToCache($tweetReach);

        return $reach;
    }

    /**
     * Returns a tweet url without its scheme and protocol
     *
     * @param  string  $tweetUrl
     * @return string
     */

    public function getTweetUrlNoSchemeProtocol($tweetUrl)
    {
        $i = strpos($tweetUrl, "twitter.com");
        return substr($tweetUrl, $i);
    }
}