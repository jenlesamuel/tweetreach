<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->singleton(TwitterApiConnection::class, function($app){
            return new TwitterOAuth(
                config("twitterapi.consumer_key"),
                config("twitterapi.consumer_secret"),
                config("twitterapi.access_token"),
                config("twitterapi.access_token_secret")
            );
        });*/
    }
}
