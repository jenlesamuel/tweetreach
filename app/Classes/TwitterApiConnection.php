<?php

namespace App\Classes;

use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * A  connection to the twitter api endpoint
 *
 * @author Jenle Samuel
 */

class TwitterApiConnection
{
    public function getConnection()
    {
        $connection =  new TwitterOAuth(
            config("twitterapi.consumer_key"),
            config("twitterapi.consumer_secret"),
            config("twitterapi.access_token"),
            config("twitterapi.access_token_secret")
        );
        $connection->setTimeouts(10, 15);

        return $connection;
    }
}