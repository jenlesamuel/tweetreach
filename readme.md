# Tweet Reach

A minimal Laravel web app that calculates the reach (the sum of the number of followers for each user that retweeted a tweet) of a tweet

## Set Up

Install project dependencies

* `$ composer install`

Run Laravel dev server

* `$ php artisan serve`



