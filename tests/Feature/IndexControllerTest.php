<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\TweetReachRepository;
use Mockery;
use Faker\Factory;
use Cache;

class IndexControllerTest extends TestCase
{

    use RefreshDatabase;

    protected $tweetReachRepositoryMock;
    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->tweetReachRepositoryMock = Mockery::mock(TweetReachRepository::class)->makePartial();
        $this->app->instance(TweetReachRepository::class, $this->tweetReachRepositoryMock);

    }

    public function tearDown()
    {
        Mockery::close();
    }


    public function testShouldReturnTheReachOfATweet()
    {
        $retweet1 = new \stdClass;
        $retweet1->user = new \stdClass;
        $retweet1->user->id = $this->faker->uuid;
        $retweet1->user->followers_count = 900;

        $retweet2 = new \stdClass;
        $retweet2->user = new \stdClass;
        $retweet2->user->id = $this->faker->uuid;
        $retweet2->user->followers_count = 200;

        $retweets = [$retweet1, $retweet2];

        $this->tweetReachRepositoryMock->shouldReceive("getRetweets")
            ->once()
            ->andReturn($retweets);

        $tweetUrl = "https://twitter.com/brk/status/974131063553";
        $urlWithoutSchemeOrProtocol = "twitter.com/brk/status/974131063553";

        $response = $this->post("/reach", ["url" => $tweetUrl]);

        $response->assertStatus(302);

        $this->assertDatabaseHas("tweet_reaches",[
                "url" => $urlWithoutSchemeOrProtocol,
                "followers" => 1100
            ]
        );

        $key = md5($urlWithoutSchemeOrProtocol);
        $this->assertTrue(Cache::has($key));

        // Remove test data from cache
        Cache::forget($key);
    }

}
