<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use App\Repositories\TweetReachRepository;
use App\Services\TweetReachService;
use Carbon\Carbon;
use Cache;
use App\TweetReach;
use Faker\Factory;

class TweetReachServiceTest extends TestCase
{
    protected $tweetReachService;
    protected $oneHourAgo;
    protected $faker;
    protected $tweetReachRepositoryMock;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->oneHourAgo = Carbon::now()->subHour()->toDateTimeString();
        $this->tweetReachRepositoryMock = Mockery::mock(TweetReachRepository::class);
        $this->app->instance(TweetReachRepository::class, $this->tweetReachRepositoryMock);

        $this->tweetReachService = resolve(TweetReachService::class);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testShouldReturnReachFromCacheBeforeExpiry()
    {
        $json = json_encode([
            "followers" => 34,
            "updated_at" => $this->oneHourAgo
        ]);

        Cache::shouldReceive("get")
            ->once()
            ->andReturn($json);

        $actual = $this->tweetReachService->getReachFromCache("url");

        $this->assertEquals(34, $actual);
    }

    public function testReturnReachFromEndpointShouldReturnTotalFollowers()
    {
        $retweet1 = new \stdClass;
        $retweet1->user = new \stdClass;
        $retweet1->user->id = $this->faker->uuid;
        $retweet1->user->followers_count = 900;

        $retweet2 = new \stdClass;
        $retweet2->user = new \stdClass;
        $retweet2->user->id = $this->faker->uuid;
        $retweet2->user->followers_count = 300;

        $retweets = [$retweet1, $retweet2];

        $this->tweetReachRepositoryMock->shouldReceive("getRetweets")
            ->once()
            ->andReturn($retweets);

        $actual = $this->tweetReachService->getReachFromEndPoint($this->faker->uuid);

        $this->assertEquals(1200, $actual);

    }

    public function testShouldReturnTweetIdFromTweetUrl()
    {
        $tweetId = 974131063553;
        $tweetUrl = "https://twitter.com/brk/status/{$tweetId}";

        $actual = $this->tweetReachService->getTweetId($tweetUrl);

        $this->assertEquals($tweetId, $actual);
    }

    public function testShouldReturnTrueIfNowExceedsAddingTwoHoursToTime()
    {
        $threeHoursAgo = Carbon::now()->subHours(3)->toDateTimeString();
        $actual = $this->tweetReachService->hasExpired($threeHoursAgo);

        $this->assertTrue($actual);
    }

    /**
     * @dataProvider urlWithoutProtocolSchemeProvider
     */

    public function testShouldReturnUrlWithoutProtocolAndScheme($url, $expected)
    {
        $actual = $this->tweetReachService->getTweetUrlNoSchemeProtocol($url);

        $this->assertEquals($expected, $actual);
    }

    public function urlWithoutProtocolSchemeProvider()
    {
        return [
            ["https://www.twitter.com/brk/status/974131063553", "twitter.com/brk/status/974131063553"],
            ["https://twitter.com/brk/status/974131063553", "twitter.com/brk/status/974131063553"],
            ["http://www.twitter.com/brk/status/974131063553", "twitter.com/brk/status/974131063553"]
        ];
    }

}
